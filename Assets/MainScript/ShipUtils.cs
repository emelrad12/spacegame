﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class ShipUtils : MonoBehaviour
{
    public GameObject weapon;
    private void Start()
    {
        string input = File.ReadAllText("./Assets/Ships/Eagle_White.json");
        Debug.Log(input);
        ShipBaseData output = JsonUtility.FromJson<ShipBaseData>(input);
        Debug.Log(output.shipStats.health);
    }
    public void spawnWeaponsOnShip(ref GameObject ship, ShipBaseData shipData)
    {
        foreach (WeaponSlotData wp in shipData.weaponSlots)
        {
            GameObject obj = Instantiate(weapon);
            obj.transform.SetParent(ship.transform);
            obj.transform.localPosition = wp.location;
        }
    }
}
public class Ship
{
    public Ship(ShipBaseData shipBase)
    {
        currentHealth = shipBase.shipStats.health ;
    }
    public float currentHealth = 0;
    public ShipBaseData baseData;
}
public class WeaponSlotData
{
    public float angle;
    public float arc;
    public int id;
    public Vector2 location;
    public string size;
    public string type;
}
[Serializable]
public class ShipStats
{
    public float turnSpeed;
    public float movementSpeed;
    public float health;
}
[Serializable]
public class ShipBaseData
{
    public int modelId;
    public ShipStats shipStats = new ShipStats();
    public List<WeaponSlotData> weaponSlots = new List<WeaponSlotData>();
}