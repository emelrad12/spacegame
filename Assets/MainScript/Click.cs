﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Click : MonoBehaviour
{
    public GameObject Missile;
    float rotationspeed = 50;
    void Update()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
            if (hit.collider != null)
            {
                Debug.Log(hit.collider.gameObject.name);
                Globals.selected = hit.collider.gameObject;
            }
        }
        if (Input.GetMouseButtonDown(1))
        {
            foreach (Transform child in Globals.selected.transform)
            {
                GameObject obj = Instantiate(Missile, child.position, child.rotation);
                Physics2D.IgnoreCollision(obj.GetComponent<CircleCollider2D>(), Globals.selected.GetComponent<PolygonCollider2D>());
                obj.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.up * 6);

            }
        }
        if(Globals.selected!=null)
        foreach (Transform child in Globals.selected.transform)
        {
            float step = rotationspeed * Time.deltaTime;
            float angle = Mathf.Atan2(mousePos2D.y - child.transform.position.y, mousePos2D.x - child.transform.position.x) * Mathf.Rad2Deg;
            Quaternion rotatefrom = child.transform.rotation;
            Quaternion rotateto = Quaternion.Euler(0f, 0f, angle - 90);
            child.transform.rotation = Quaternion.RotateTowards(rotatefrom, rotateto, step);
        }
    }
}
