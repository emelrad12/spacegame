﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainInput : MonoBehaviour
{
    private void FixedUpdate()
    {
        if (Globals.selected != null)
        {
            Rigidbody2D body = Globals.selected.GetComponent("Rigidbody2D") as Rigidbody2D;
            float moveHorizontal = -Input.GetAxis("Horizontal");
            float moveVertical = Input.GetAxis("Vertical");
            if (Globals.selected != null)
            {
                Vector2 movement = new Vector2(0f, moveVertical);
                body.AddRelativeForce(movement);
                if (Mathf.Abs(body.angularVelocity + moveHorizontal) < 20)
                {
                    Debug.Log(moveHorizontal);
                    body.AddTorque(moveHorizontal);
                }
            }
        }
    }
}
