﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    void Update()
    {
        if (Globals.selected != null)
        {
            if (Globals.CameraMode == 1)
            {
                Vector3 newCameraPosition = Globals.selected.transform.position;
                newCameraPosition.z += Globals.cameraHeight;
                transform.position = newCameraPosition;
            }
        }
        
    }
}
